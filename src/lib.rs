#[macro_use]
extern crate display_derive;
extern crate failure;
extern crate reqwest;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;

use std::cmp::Ordering::Equal;

use failure::{err_msg, Error};
use reqwest::Url;

pub const RPC_ADDR: &'static str = "https://aur.archlinux.org/rpc/";

#[derive(Deserialize, Debug)]
pub struct RaurResult {
    #[serde(rename = "type")]
    response_type: String,

    error: Option<String>,

    #[serde(rename = "results")]
    pub packages: Vec<Package>
}

impl RaurResult {
    pub fn count(&self) -> usize {
        self.packages.len()
    }

    pub fn pop(&mut self) -> Option<Package> {
        self.packages.pop()
    }

    fn sort(&mut self) {
        self.packages.sort_by(
            |a, b| a.popularity.partial_cmp(&b.popularity).unwrap_or(Equal)
        );
    }
}

#[derive(Clone, Deserialize, Debug, PartialEq, Serialize)]
pub struct Package {
    #[serde(rename = "ID")]
    pub id: i32,

    #[serde(rename = "Name")]
    pub name: String,

    #[serde(rename = "PackageBaseID")]
    pub package_base_id: Option<i32>,

    #[serde(rename = "PackageBase")]
    pub package_base: Option<String>,

    #[serde(rename = "Version")]
    pub version: String,

    #[serde(rename = "Description")]
    pub description: Option<String>,

    /// Package source code url
    #[serde(rename = "URL")]
    pub url: Option<String>,

    #[serde(rename = "NumVotes")]
    pub num_votes: i32,

    #[serde(rename = "Popularity")]
    pub popularity: f32,

    #[serde(rename = "OutOfDate")]
    pub out_of_date: Option<i32>,

    #[serde(rename = "Maintainer")]
    pub maintainer: Option<String>,

    #[serde(rename = "FirstSubmitted")]
    pub first_submitted: Option<i32>,

    #[serde(rename = "LastModified")]
    pub last_modified: Option<i32>,

    #[serde(rename = "URLPath")]
    pub pkgbuild_url: String,

    #[serde(rename = "Depends")]
    pub dependencies: Option<Vec<String>>,

    #[serde(rename = "MakeDepends")]
    pub make_dependencies: Option<Vec<String>>,
    
    #[serde(rename = "License")]
    pub license: Option<Vec<String>>,

    #[serde(rename = "Keywords")]
    pub keywords: Option<Vec<String>>
}

/// Describes a way the AUR should handle a search query
#[derive(Display)]
pub enum SearchStrategy {
    /// search by package name only
    #[display(fmt = "name")]
    Name,

    /// search by package name and description (default)
    #[display(fmt = "name-desc")]
    NameDescription,

    /// search by package maintainer
    #[display(fmt = "maintainer")]
    Maintainer,

    /// search for packages that depend on the query
    #[display(fmt = "depends")]
    Dependency,

    /// search for packages that makedepend on the query
    #[display(fmt = "makedepends")]
    MakeDependency,

    /// search for packages that optdepend on the query
    #[display(fmt = "optdepends")]
    OptionalDependency,

    /// search for packages that checkdepend on the query
    #[display(fmt = "checkdepends")]
    CheckDependency
}

/// Use the AUR RPC `search` functionality. This differs from [`info`](fn.info.html) function in that
/// it will search using a query coupled with a [`SearchStrategy`](enum.SearchStrategy.html).
/// This function's RaurResult's list of results is sorted by popularity.
pub fn search<S: AsRef<str>>(query: S, strategy: SearchStrategy) -> Result<RaurResult, Error> {
    let url = Url::parse_with_params(RPC_ADDR,
                                      &[("v", "5"),
                                        ("type", "search"),
                                        ("by", &format!("{}", strategy)),
                                        ("arg", query.as_ref())])?;
    request(url, true)
}

/// Use the AUR RPC `info` functionality. This differs from the [`search`](fn.search.html)
/// function in that it takes a list (TBA) of exact matches of package names and returns
/// their info.
/// Ownership of the names of any packages that are not found is passed back to the caller.
pub fn info(mut pkg_names: Vec<String>) -> Result<(RaurResult, Vec<String>), Error> {
    let url = {
        let mut params = pkg_names
            .iter()
            .map(|name| ("arg[]", name.as_ref()))
            .collect::<Vec<(&str, &str)>>();
        params.extend(&[("v", "5"), ("type", "info")]);

        Url::parse_with_params(RPC_ADDR, &params)?
    };
    let result = request(url, false)?;
    let unmatched = {
        let mut results = result.packages.iter();
        let mut current = results.next();
        // RPC sorts all it's packages in alphabetical order
        //BUG: This might produce undefined behavior for a few corner cases
        //  Why?
        pkg_names.sort_unstable();
        let (_, unmatched): (_, Vec<String>) = pkg_names.drain(..)
            .partition(|name| {
                if let Some(peek) = current {
                    if name == &peek.name {
                        current = results.next();
                        true
                    } else {
                        false
                    }
                } else {
                    false
                }
            });
        unmatched
    };
    Ok((result, unmatched))
}

fn request(url: Url, sort: bool) -> Result<RaurResult, Error> {
    let mut result: RaurResult = reqwest::get(url)?
        .json()?;

    if sort {
        result.sort();
    }

    for pkg in result.packages.iter_mut() {
        pkg.pkgbuild_url = Url::parse(RPC_ADDR)?
            .join(&pkg.pkgbuild_url)?
            .into_string();
    }

    if result.response_type == "error" {
        Err(err_msg(result.error.expect("That's Weird. Contact raur developers")))
    } else {
        Ok(result)
    }
}

//TODO: MOAR!
#[cfg(test)]
mod tests {
    use {info, search, SearchStrategy};

    #[test]
    fn test_info() {
        info(vec!["intellij-idea-ce".to_string()]).unwrap();
        // Also tests sorting, since requests are returned in alphabetical order
        let (mut result, _) = info(vec!["swift".to_string(), "mininim".to_string()]).unwrap();

        let swift = result.pop().unwrap();
        let mininim = result.pop().unwrap();
        assert!(swift.popularity > mininim.popularity);
    }

    #[test]
    fn test_search() {
        let result = search(&String::from("zzzzzzzzzzz"), SearchStrategy::NameDescription).unwrap();
        assert_eq!(0, result.count());

        let mut query_res = search(&String::from("spotify"), SearchStrategy::NameDescription).unwrap();
        assert!(query_res.count() > 0);

        let a = query_res.pop().unwrap();
        let b = query_res.pop().unwrap();
        assert!(a.popularity > b.popularity);
    }
}
