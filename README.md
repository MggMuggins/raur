[![Crates.io](https://img.shields.io/crates/v/raur.svg)](https://crates.io/crates/raur) [![pipeline status](https://gitlab.com/davidbittner/paurc/badges/master/pipeline.svg)](https://gitlab.com/davidbittner/paurc/commits/master)
# raur
A thin rust wrapper over the AUR RPC Json interface. Documentation is a little sparse, refer to the [ArchWiki RPC Docs](https://wiki.archlinux.org/index.php/Aurweb_RPC_interface) for more info on that API. Documentation for this crate is available on crates.io

This crate tries to implment the whole RPC Json API not to assume use cases.

## Usage
```Rust
use raur::{search, SearchStrategy}

// Use `search` to search using keywords (multiple strategies available)
let results = search("spotify", SearchStrategy::NameDescription)?;

// Use `info` to get info about a specific package name.
let results = info(&["spotify"])?;
```
